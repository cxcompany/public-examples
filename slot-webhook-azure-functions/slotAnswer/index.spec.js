const fn = require('./index')

let mockedAnswer = {
    log: jest.fn()
}
let mockedReq

describe('Slot Validation', () => {
    describe('Unit:', () => {
        beforeEach(() => {
            mockedReq = {
                'originalInputText': 'string',
                'normalizedInputText': 'string',
                'inputPatternMatch': 'string',
                'inputSlotName': 'string',
                'dialogName': 'string',
                'culture': 'string',
                'sessionId': 'string',
                'slot': {
                    'name': 'string',
                    'inputPatterns': [
                        'string'
                    ],
                    'missingValueMessage': 'string',
                    'okMessage': 'string',
                    'errorMessage': 'string',
                    'quickReplies': [
                        {
                            'text': 'string'
                        }
                    ],
                    'value': 'string',
                    'metadata': {}
                }

            }
        })

        it('should have as header application/json', async (done) => {
            await fn(mockedAnswer, { body: mockedReq })

            expect(mockedAnswer.res.headers).toMatchSnapshot()
            done()
        })

        it('should always answer 200', async (done) => {
            await fn(mockedAnswer, { body: mockedReq })

            expect(mockedAnswer.res.status).toMatchSnapshot()
            done()
        })

        it('should return the status pending when the slot is not the current one', async (done) => {
            mockedReq.slot.name = 'hello'
            mockedReq.inputSlotName = 'world'
            await fn(mockedAnswer, { body: mockedReq })

            expect(mockedAnswer.res.body).toMatchSnapshot()
            done()
        })

        it('should return the status done when the slot is the correct one', async (done) => {
            await fn(mockedAnswer, { body: mockedReq })

            expect(mockedAnswer.res.body).toMatchSnapshot()
            done()
        })
    })
})
