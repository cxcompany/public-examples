module.exports = async (answer, req) => {
    answer.log('JavaScript HTTP trigger function processed a request.')

    answer.res = {}
    // NOTE: Answer always JSON 20190306:Alevale
    answer.res.status = 200
    answer.res.headers = {
        'Content-Type': 'application/json'
    }

    const {
        originalInputText,
        inputSlotName,
        slot
    } = req.body

    const {
        missingValueMessage,
        okMessage,
        errorMessage,
        metadata,
        quickReplies
    } = slot

    // NOTE: Not in current slot, the slot is pending 20190318:Alevale
    if (inputSlotName !== slot.name) {
        answer.res.body = {
            success: true,
            error: '',
            result: {
                quickReplies,
                message: {
                    text: missingValueMessage
                },
                value: '',
                status: 'pending',
                metadata
            }
        }
    // NOTE: There is no custom logic here, just always validating 20190318:Alevale
    } else {
        answer.res.body = {
            'success': true,
            'error': '',
            'result': {
                'message': {
                    'text': okMessage
                },
                'value': JSON.stringify(originalInputText),
                'status': 'done',
                quickReplies,
                metadata
            }
        }
    }
}
