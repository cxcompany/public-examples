# Introduction

This repository contains the example code that we share with our clients/partners

To use an example do the following

- git clone this repo
- select the example folder for the usecase you want to implement
- implement your own logic, or copy ours
- deploy to the service that the demo was built for (There will be guidelines in the project itself)

Supported technologies:

- Azure functions
- MS Azure web app
