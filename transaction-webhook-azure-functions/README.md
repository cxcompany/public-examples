# Introduction

This has been created following the examples provided for the azure functions.

You can find the documentation [here](https://docs.microsoft.com/en-us/azure/azure-functions/functions-run-local)


## Development

To develop we assume that you have the required tools to make the azure functions work locally [more details here](https://docs.microsoft.com/en-us/azure/azure-functions/)

Install all the dependencies `npm i`

Once in this folder there are 2 commands that you can run.

If you want to do TDD you can use the command `npm run test:watch`

If you want to just run the code and see that it works in the browser just run in your favorite terminal `func host start`

## Deployment

To deploy from this folder the only thing that you need to do is to add the remote GIT repo for the azure function that you have in your azure portal and just `git push` to that remote all of the files in this folder.

# Data contract

## Inbound call

This is the call that the engine makes to you.
It makes one call for each transaction you have. You need to have the URL's specified in
the CMS so that the engine knows what should call in each interaction.

This is an example payload

```json
{
  "dialogName": "string",
  "okMessage": "string",
  "errorMessage": "string",
  "culture": "string",
  "sessionId": "string",
  "slots": [
    {
      "name": "string",
      "inputPatterns": [
        "string"
      ],
      "missingValueMessage": "string",
      "okMessage": "string",
      "errorMessage": "string",
      "quickReplies": [
        {
          "text": "string"
        }
      ],
      "value": "string",
      "metadata": {}
    }
  ]
}
```

## Outbound call

This is what you need to answer to the Engine for the API call

This is the payload, if any of the properties is missing, **the whole webhook** would fail

```json
{
  "success": true,
  "error": "string",
  "result": {
    "message": {
      "text": "string"
    },
    "resetSlotStatus": [
      "string"
    ]
  }
}
```
