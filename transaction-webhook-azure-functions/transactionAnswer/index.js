module.exports = async (answer, req) => {
    answer.log('JavaScript HTTP trigger function processed a request.')

    answer.res = {}
    // NOTE: Answer always JSON 20190306:Alevale
    answer.res.status = 200
    answer.res.headers = {
        'Content-Type': 'application/json'
    }

    const {
        okMessage
    } = req.body

    // NOTE: There is no custom logic here, it is always valid 20190318:Alevale
    answer.res.body = {
        'success': true,
        'error': '',
        'result': {
            'message': {
                'text': okMessage
            },
            'resetSlotStatus': []
        }
    }
}
