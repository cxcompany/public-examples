const fn = require('./index')

let mockedAnswer = {
    log: jest.fn()
}
let mockedReq

describe('Transaction Validation', () => {
    describe('Unit:', () => {
        beforeEach(() => {
            mockedReq = {
                'dialogName': 'string',
                'okMessage': 'string',
                'errorMessage': 'string',
                'culture': 'string',
                'sessionId': 'string',
                'slots': [
                    {
                        'name': 'string',
                        'inputPatterns': [
                            'string'
                        ],
                        'missingValueMessage': 'string',
                        'okMessage': 'string',
                        'errorMessage': 'string',
                        'quickReplies': [
                            {
                                'text': 'string'
                            }
                        ],
                        'value': 'string',
                        'metadata': {}
                    }
                ]
            }
        })

        it('should have as header application/json', async (done) => {
            await fn(mockedAnswer, { body: mockedReq })

            expect(mockedAnswer.res.headers).toMatchSnapshot()
            done()
        })

        it('should always answer 200', async (done) => {
            await fn(mockedAnswer, { body: mockedReq })

            expect(mockedAnswer.res.status).toMatchSnapshot()
            done()
        })

        it('should return the payload by default', async (done) => {
            await fn(mockedAnswer, { body: mockedReq })

            expect(mockedAnswer.res.body).toMatchSnapshot()
            done()
        })
    })
})
