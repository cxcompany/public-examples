# Introduction

This has been created following the examples provided for the azure functions.

You can find the documentation [here](https://docs.microsoft.com/en-us/azure/azure-functions/functions-run-local)


## Development

To develop we assume that you have the required tools to make the azure functions work locally [more details here](https://docs.microsoft.com/en-us/azure/azure-functions/)

Install all the dependencies `npm i`

Once in this folder there are 2 commands that you can run.

If you want to do TDD you can use the command `npm run test:watch`

If you want to just run the code and see that it works in the browser just run in your favorite terminal `func host start`

## Deployment

To deploy from this folder the only thing that you need to do is to add the remote GIT repo for the azure function that you have in your azure portal and just `git push` to that remote all of the files in this folder.

# Data contract

## Inbound call

This is the call that the engine makes to you.
It only makes one call for all of the contextVariables

This is an example payload

```json
{
  "correlationId": "56DAC6BF-2F6E-454F-BC3A-FCD953EEE256",
  "input": {
    "type": "qa",
    "value": "heya there",
    "normalizedValue": "heya there"
  },
  "culture": "en",
  "user": {
    "user-key1": "user-value1",
    "user-key2": "user-value2",
    "user-key3": "user-value3"
  },
  "session": {
    "session-key1": "session-value1"
  },
  "contextVariables": [{
    "key": "livechatAvailable",
    "value": "yes",
    "allowedValues": ["yes", "no"]
  }],
  "request": {
    "location": "https://engine",
    "queryParams": [{
      "queryParam-key1": "queryParam-value1",
      "queryParam-key2": "queryParam-value2"
    }]
  }
}
```

## Outbound call

This is what you need to answer to the Engine for the contextVariable API call
It's only able to allow values that were already in the CMS

This is the payload

```json
{
    "contextVariables": [{
        "key": "livechatAvailable",
        "value": "yes"
    }]
}
```
