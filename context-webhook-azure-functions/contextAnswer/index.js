/**
 * Gets the contextVariables that were passed in, and returns their value if it's in the allowed values
 * if the value doesn't exist in the allowedValues it returns an empty value for that contextVariable
 * @param req
 * @returns {{key: *, value: string}[]}
 */
const getContextVariablesPassed = (req) => {
    return req.body.contextVariables.map((contextVariable) => {
        if (!contextVariable.allowedValues.includes(contextVariable.value)) {
            contextVariable.value = null
        }
        return {
            key: contextVariable.key,
            value: contextVariable.value
        }
    })
}

module.exports = async (answer, req) => {
    answer.log('JavaScript HTTP trigger function processed a request.')

    answer.res = {}
    // NOTE: Answer always JSON 20190306:Alevale
    answer.res.headers = {
        'Content-Type': 'application/json'
    }

    if (req.body && req.body.contextVariables) {
        const contextVariables = getContextVariablesPassed(req)

        answer.res.status = 200
        answer.res.body = {
            contextVariables
        }

    } else {

        answer.res.status = 400
        answer.res.body = {
            error: 'Missing contextVariables on the request'
        }
    }
}
