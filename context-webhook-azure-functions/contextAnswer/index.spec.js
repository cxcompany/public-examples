const fn = require('./index')

let mockedAnswer = {
    log: jest.fn()
}
let mockedReq

describe('Context Variables', () => {
    describe('Unit:', () => {
        beforeEach(() => {
            mockedReq = {
                'correlationId': '56DAC6BF-2F6E-454F-BC3A-FCD953EEE256',
                'input': {
                    'type': 'qa',
                    'value': 'heya there',
                    'normalizedValue': 'heya there'
                },
                'culture': 'en',
                'user': {
                    'user-key1': 'user-value1'
                },
                'session': {
                    'session-key1': 'session-value1'
                },
                'contextVariables': [{
                    'key': 'livechatAvailable',
                    'value': 'yes',
                    'allowedValues': ['yes', 'no']
                }],
                'request': {
                    'location': 'https://engine',
                    'queryParams': [{
                        'queryParam-key1': 'queryParam-value1',
                        'queryParam-key2': 'queryParam-value2'
                    }]
                }
            }
        })

        it('should have as header application/json', async (done) => {
            await fn(mockedAnswer, {body: mockedReq})

            expect(mockedAnswer.res.headers).toMatchSnapshot()
            done()
        })

        it('should answer 200 when the request is correct', async (done) => {
            await fn(mockedAnswer, {body: mockedReq})

            expect(mockedAnswer.res.status).toMatchSnapshot()
            done()
        })

        it('should return the same contextVariables that were sent in', async (done) => {
            await fn(mockedAnswer, {body: mockedReq})

            expect(mockedAnswer.res.body).toMatchSnapshot()
            done()
        })

        it('should return null when the contextVariable passed is not in the allowedValues', async (done) => {
            mockedReq.contextVariables = [{
                'key': 'livechatAvailable',
                'value': 'yayn',
                'allowedValues': ['yes', 'no']
            }]

            await fn(mockedAnswer, {body: mockedReq})

            expect(mockedAnswer.res.body).toMatchSnapshot()
            done()
        })

        it('should allow to return multiple contextVariables', async (done) => {
            mockedReq.contextVariables = [{
                'key': 'livechatAvailable',
                'value': 'yes',
                'allowedValues': ['yes', 'no']
            }, {
                'key': 'agent',
                'value': '33',
                'allowedValues': ['33', '44']
            }, {
                'key': 'time',
                'value': 'day',
                'allowedValues': ['day', 'night']
            }]

            await fn(mockedAnswer, {body: mockedReq})

            expect(mockedAnswer.res.body).toMatchSnapshot()
            done()
        })

        it('should return an error message when no contextVariables were sent', async (done) => {
            delete mockedReq.contextVariables

            await fn(mockedAnswer, {body: mockedReq})

            expect(mockedAnswer.res.body).toMatchSnapshot()
            done()
        })
    })
})
