const app = require('./src/app')

const port = process.env.PORT || 8080

const server = app.listen(port, function startApp () {
    let host = this.address().address
    host = (host === '::') ? '[::1]' : host
    console.log('DCX webhooks now listening at http://%s:%s', host, port)
})

module.exports = server
