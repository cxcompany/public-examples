const app = require('../../test-app')
const url = '/context-variables/validate'

let mockedContext

describe('Context Variables', () => {
    describe('Unit:', () => {
        beforeEach(() => {
            mockedContext = {
                'correlationId': '56DAC6BF-2F6E-454F-BC3A-FCD953EEE256',
                'input': {
                    'type': 'qa',
                    'value': 'heya there',
                    'normalizedValue': 'heya there'
                },
                'culture': 'en',
                'user': {
                    'user-key1': 'user-value1'
                },
                'session': {
                    'session-key1': 'session-value1'
                },
                'contextVariables': [{
                    'key': 'livechatAvailable',
                    'value': 'yes',
                    'allowedValues': ['yes', 'no']
                }],
                'request': {
                    'location': 'https://engine',
                    'queryParams': [{
                        'queryParam-key1': 'queryParam-value1',
                        'queryParam-key2': 'queryParam-value2'
                    }]
                }
            }
        })

        it('should return the same contextVariables that were sent in', async (done) => {
            const response = await app.post(url).send(mockedContext)

            expect(response.body).toMatchSnapshot()
            done()
        })

        it('should return the contextVariable empty when its not in the allowed values', async (done) => {
            mockedContext.contextVariables = [{
                'key': 'livechatAvailable',
                'value': 'yayn',
                'allowedValues': ['yes', 'no']
            }]

            const response = await app.post(url).send(mockedContext)

            expect(response.body).toMatchSnapshot()
            done()
        })

        it('should return multiple contextVariables when passed in', async (done) => {
            mockedContext.contextVariables = [{
                'key': 'livechatAvailable',
                'value': 'yes',
                'allowedValues': ['yes', 'no']
            }, {
                'key': 'agent',
                'value': '33',
                'allowedValues': ['33', '44']
            }, {
                'key': 'time',
                'value': 'day',
                'allowedValues': ['day', 'night']
            }]

            const response = await app.post(url).send(mockedContext)

            expect(response.body).toMatchSnapshot()
            done()
        })

        it('should return an error message when no contextVariable was sent in', async (done) => {
            delete mockedContext.contextVariables
            const response = await app.post(url).send(mockedContext)

            expect(response.body).toMatchSnapshot()
            done()
        })
    })
})
