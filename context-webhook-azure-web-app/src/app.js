const express = require('express')
const bodyParser = require('body-parser')

const ping = require('./routes/ping')
const contextVariables = require('./contextVariables')

const app = express()

app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true }))

app.use('/context-variables', contextVariables)

app.get('/', ping)

module.exports = app
