const router = require('express').Router()
const validate = require('./validate')

router.post(
    '/validate',
    validate
)

module.exports = router
