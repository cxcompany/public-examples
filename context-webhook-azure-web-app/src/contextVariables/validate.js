/**
 * Gets the contextVariables that were passed in, and returns their value if it's in the allowed values
 * if the value doesn't exist in the allowedValues it returns an empty value for that contextVariable
 * @param req
 * @returns {{key: *, value: string}[]}
 */
const getContextVariablesPassed = (req) => {
    return req.body.contextVariables.map((contextVariable) => {
        if (!contextVariable.allowedValues.includes(contextVariable.value)) {
            contextVariable.value = null
        }
        return {
            key: contextVariable.key,
            value: contextVariable.value
        }
    })
}

module.exports = (req, res, next) => {
    if (req.body && req.body.contextVariables) {
        const contextVariables = getContextVariablesPassed(req)

        return res.send({
            contextVariables
        })
    }

    return res.send(400, {error: 'Missing contextVariables on the request'})
}
