module.exports = {
    "extends": [
        "standard",
        "plugin:jest/recommended"
    ],
    "rules": {
        "indent": ["error", 4],
        "operator-linebreak": ["error", "before"]
    },
    "plugins": ["jest"],
    "env": {
        "jest/globals": true
    }

};
